//
//  ViewController.swift
//  Day24Project4Challenge
//
//  Created by Andrew Famiano on 3/4/20.
//  Copyright © 2020 Waystocreate. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    var websites = ["mazdausa", "apple", "jeep", "bmwusa", "chevy", "ford"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Websites"
        // Do any additional setup after loading the view.
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return websites.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Websites", for: indexPath)
        cell.textLabel?.text = websites[indexPath.row].capitalized
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "Web") as? WebViewController {
            
            vc.websites = websites[indexPath.row]
            
            navigationController?.pushViewController(vc, animated: true)
        }
        
    }


}

